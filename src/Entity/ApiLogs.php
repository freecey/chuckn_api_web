<?php

namespace App\Entity;

use App\Repository\ApiLogsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiLogsRepository::class)
 */
class ApiLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $requestDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlApi;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $json_in;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $json_out;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $responseCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ResponseDate;

    /**
     * @ORM\ManyToMany(targetEntity=Jokes::class, inversedBy="apiLogs")
     */
    private $joke;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pathApi;

    public function __construct()
    {
        $this->joke = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequestDate(): ?\DateTimeInterface
    {
        return $this->requestDate;
    }

    public function setRequestDate(\DateTimeInterface $requestDate): self
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getUrlApi(): ?string
    {
        return $this->urlApi;
    }

    public function setUrlApi(string $urlApi): self
    {
        $this->urlApi = $urlApi;

        return $this;
    }

    public function getJsonIn(): ?string
    {
        return $this->json_in;
    }

    public function setJsonIn(?string $json_in): self
    {
        $this->json_in = $json_in;

        return $this;
    }

    public function getJsonOut(): ?string
    {
        return $this->json_out;
    }

    public function setJsonOut(?string $json_out): self
    {
        $this->json_out = $json_out;

        return $this;
    }

    public function getResponseCode(): ?int
    {
        return $this->responseCode;
    }

    public function setResponseCode(?int $responseCode): self
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    public function getResponseDate(): ?\DateTimeInterface
    {
        return $this->ResponseDate;
    }

    public function setResponseDate(?\DateTimeInterface $ResponseDate): self
    {
        $this->ResponseDate = $ResponseDate;

        return $this;
    }

    /**
     * @return Collection<int, Jokes>
     */
    public function getJoke(): Collection
    {
        return $this->joke;
    }

    public function addJoke(Jokes $joke): self
    {
        if (!$this->joke->contains($joke)) {
            $this->joke[] = $joke;
        }

        return $this;
    }

    public function removeJoke(Jokes $joke): self
    {
        $this->joke->removeElement($joke);

        return $this;
    }

    public function getPathApi(): ?string
    {
        return $this->pathApi;
    }

    public function setPathApi(?string $pathApi): self
    {
        $this->pathApi = $pathApi;

        return $this;
    }
}
