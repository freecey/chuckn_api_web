<?php

namespace App\Twig\Extension;

use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class DatabaseGlobalsExtension extends AbstractExtension implements GlobalsInterface
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getGlobals(): array
    {

        $setting = $this->em->getRepository(\App\Entity\Setting::class)->findFirstRecord();
        $setting = $setting[0];
        $count_facts = $this->em->getRepository(\App\Entity\Jokes::class)->count([]);
        return [
            'myVariable' => 'TEST FURURE SETTING',
            'mySetting' => $setting,
            'count_facts' => $count_facts,
//            'mySetting' => $this->em->getRepository(FuzAppBundle\Entity::class)->getSomeData(),
        ];
    }
}