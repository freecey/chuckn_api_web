<?php

namespace App\Controller\Admin;

use App\Repository\ApiLogsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminApiLogsController extends AbstractController
{
    #[Route('/admin/api_logs', name: 'admin.api_logs.index')]
    public function index(Request $request, PaginatorInterface $paginator ,ApiLogsRepository $logsRepository): Response
    {

        $SearchLogs = $logsRepository->findBy(
            array(),
            array('id' => 'DESC')
        );

        $allLogs = $paginator->paginate(
            $SearchLogs,
            $request->query->getInt('page', 1),
            20
        );
        return $this->render('admin/api_logs/index.html.twig', [
            'controller_name' => 'AdminApiLogsController',
            'ApiLogs' => $allLogs,
        ]);
    }
}
