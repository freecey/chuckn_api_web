<?php

namespace App\Controller\Admin;

use App\Entity\Setting;
use App\Form\SettingType;
use App\Repository\FetchLogsRepository;
use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSettingController extends AbstractController
{
    #[Route('/admin/setting', name: 'admin.setting')]
    public function index(Request $request, EntityManagerInterface $manager, SettingRepository $settingRepository, FetchLogsRepository $logsRepository): Response
    {
        $setting = $settingRepository->findFirstRecord();
        $LastFetchLog = $logsRepository->findBy(array(), array('id' => 'DESC'),5);
        if ($setting == null)
        {
            $setting = new Setting();
        }
        else {
            $setting = $setting[0];
        }

        $form = $this->createForm(SettingType::class, $setting);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($setting);
            $manager->flush();
            $this->addFlash('success', 'Setting Updated, Successfully');
        }

        return $this->render('admin/setting/index.html.twig', [
            'controller_name' => 'AdminSettingController',
            'SettingForm' => $form->createView(),
            'FetchLog' => $LastFetchLog,
        ]);
    }
}
