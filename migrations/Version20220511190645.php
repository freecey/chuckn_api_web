<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220511190645 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE api_logs_jokes (api_logs_id INT NOT NULL, jokes_id INT NOT NULL, INDEX IDX_B39474C6E7E880A (api_logs_id), INDEX IDX_B39474C68A9C4BE2 (jokes_id), PRIMARY KEY(api_logs_id, jokes_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE api_logs_jokes ADD CONSTRAINT FK_B39474C6E7E880A FOREIGN KEY (api_logs_id) REFERENCES api_logs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE api_logs_jokes ADD CONSTRAINT FK_B39474C68A9C4BE2 FOREIGN KEY (jokes_id) REFERENCES jokes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE api_logs ADD response_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE api_logs_jokes');
        $this->addSql('ALTER TABLE api_logs DROP response_date');
    }
}
