<?php

namespace App\Form;

use App\Entity\NewJokes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddBulksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nmb_joke', IntegerType::class, [
                'mapped' => false,
                'label' => 'Nmb for add in Bulks',
            ])
            ->add('Submit', SubmitType::class, [
                'label' => 'add in Bulks',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}
