<?php

namespace App\Controller\Admin;

use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminContactController extends AbstractController
{
    #[Route('/admin/contact', name: 'admin.contact.index')]
    public function index(Request $request, ContactRepository $contactRepository, PaginatorInterface $paginator): Response
    {
        $SearchContact = $contactRepository->findBy(
            array(),
            array('id' => 'DESC')
        );

        $AllContact = $paginator->paginate(
            $SearchContact, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );

        return $this->render('admin/contact/index.html.twig', [
            'controller_name' => 'AdminContactController',
            'AllContact' => $AllContact,
        ]);
    }

    #[Route('/admin/contact/read/{id}', name: 'admin.contact.read', methods: ['POST',])]
    public function read(int $id, Request $request, ContactRepository $contactRepository, EntityManagerInterface $manager): Response
    {
        $contact = $contactRepository->findOneBy(['id' => $id]);
        $content = $contact->getName() . ' - ';
        $content .= ''.$contact->getEmail() . '<br>';
        $content .= $contact->getCreatedAt()->format('Y-m-d - h:i:s') . '<br>';
        $content .= 'ip: '. $contact->getIp(). '[[%CUT%]]';
        $content .= $contact->getMessage() . '<br>';

        $contact->setReaded(true);
        $manager->persist($contact);
        $manager->flush();
//        { \'body\' : \'MSG ID '. $id .' RAND : '. rand(1, 500) .'<br>' .
        return new Response($content);

    }
}
