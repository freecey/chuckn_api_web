<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact.index')]
    public function index(Request $request, EntityManagerInterface $manager, MailerInterface $mailer): Response
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();
            $contact->setCreatedAt(new \DateTime());
            $contact->setReaded(false);
            $contact->setSend(false);
            $contact->setIp($request->getClientIp());
            $manager->persist($contact);
            $manager->flush();

            $email = (new Email())
//                ->from('hello@example.com')
                ->to('contact@neant.be')
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('CHUCKN Contact Form ' . $contact->getId())
                ->text(
                    'Name: ' . $contact->getName() . '
' .
                    'Mail: ' . $contact->getEmail() . '

' .
                    'Message:
' . $contact->getMessage() . '

' .
                    date_format($contact->getCreatedAt(), 'Y-m-d H:i:s'))
                ->html('<p>Name: ' . $contact->getName() . '</p>' .
                    '<p>Mail: ' . $contact->getEmail() . '</p>' .
                    '<p>Message:<br>' . $contact->getMessage() . '</p>' .
                    '<p>' . date_format($contact->getCreatedAt(), 'Y-m-d H:i:s') . '</p>');
            try {
                $mailer->send($email);
                $contact->setSend(true);
                $manager->persist($contact);
                $manager->flush();
            } catch (TransportExceptionInterface $e) {
                // prevent to display error, contact message saved in db
            }

            return $this->render('contact/merci.html.twig', [
                'controller_name' => 'ContactController',
            ]);
        }


        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'form' => $form->createView(),
        ]);
    }

//    #[Route('/contact/merci', name: 'contact.merci')]
//    public function merci(Request $request): Response
//    {
//
//    }
}
