<?php

namespace App\Entity;

use App\Repository\SettingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SettingRepository::class)
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $sitename;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email_contact;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fetchUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fetchIdStart;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fetchIdEnd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fetchIdLast;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $snippetsHeader;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $snippetsFooter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $snippetsHeaderEnable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $snippetsFooterEnable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSitename(): ?string
    {
        return $this->sitename;
    }

    public function setSitename(?string $sitename): self
    {
        $this->sitename = $sitename;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getEmailContact(): ?string
    {
        return $this->email_contact;
    }

    public function setEmailContact(?string $email_contact): self
    {
        $this->email_contact = $email_contact;

        return $this;
    }

    public function getFetchUrl(): ?string
    {
        return $this->fetchUrl;
    }

    public function setFetchUrl(?string $fetchUrl): self
    {
        $this->fetchUrl = $fetchUrl;

        return $this;
    }

    public function getFetchIdStart(): ?int
    {
        return $this->fetchIdStart;
    }

    public function setFetchIdStart(?int $fetchIdStart): self
    {
        $this->fetchIdStart = $fetchIdStart;

        return $this;
    }

    public function getFetchIdEnd(): ?int
    {
        return $this->fetchIdEnd;
    }

    public function setFetchIdEnd(?int $fetchIdEnd): self
    {
        $this->fetchIdEnd = $fetchIdEnd;

        return $this;
    }

    public function getFetchIdLast(): ?int
    {
        return $this->fetchIdLast;
    }

    public function setFetchIdLast(?int $fetchIdLast): self
    {
        $this->fetchIdLast = $fetchIdLast;

        return $this;
    }

    public function getSnippetsHeader(): ?string
    {
        return $this->snippetsHeader;
    }

    public function setSnippetsHeader(?string $snippetsHeader): self
    {
        $this->snippetsHeader = $snippetsHeader;

        return $this;
    }

    public function getSnippetsFooter(): ?string
    {
        return $this->snippetsFooter;
    }

    public function setSnippetsFooter(?string $snippetsFooter): self
    {
        $this->snippetsFooter = $snippetsFooter;

        return $this;
    }

    public function isSnippetsHeaderEnable(): ?bool
    {
        return $this->snippetsHeaderEnable;
    }

    public function setSnippetsHeaderEnable(?bool $snippetsHeaderEnable): self
    {
        $this->snippetsHeaderEnable = $snippetsHeaderEnable;

        return $this;
    }

    public function isSnippetsFooterEnable(): ?bool
    {
        return $this->snippetsFooterEnable;
    }

    public function setSnippetsFooterEnable(?bool $snippetsFooterEnable): self
    {
        $this->snippetsFooterEnable = $snippetsFooterEnable;

        return $this;
    }
}
