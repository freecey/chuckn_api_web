<?php

namespace App\Form;

use App\Entity\NewJokes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewJokesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('joke', TextareaType::class, [
                'label' => 'Votre Fact',
                'attr' => [
                    'rows' => 4,
                ],
            ])
//            ->add('created_at')
//            ->add('vote')
            ->add('Submit', SubmitType::class, [
                'label' => 'Envoyer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NewJokes::class,
        ]);
    }
}
