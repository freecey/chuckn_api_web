<?php

namespace App\Controller\Admin;

use App\Entity\FetchData;
use App\Entity\FetchLogs;
use App\Entity\NewJokes;
use App\Entity\Setting;
use App\Form\FetchDataType;
use App\Form\SettingType;
use App\Repository\FetchDataRepository;
use App\Repository\FetchLogsRepository;
use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminFetchDataController extends AbstractController
{
    #[Route('/admin/fetch_data', name: 'admin.fetch_data.index')]
    public function index(Request $request, EntityManagerInterface $manager, SettingRepository $settingRepository, FetchLogsRepository $logsRepository): Response
    {
        $setting = $settingRepository->findFirstRecord();
        $LastFetchLog = $logsRepository->findBy(array(), array('id' => 'DESC'),5);
        if ($setting == null)
        {
            $setting = new Setting();
        }
        else {
            $setting = $setting[0];
        }

        $form = $this->createForm(FetchDataType::class, $setting);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($setting);
            $manager->flush();
            $this->addFlash('success', 'Setting Updated, Successfully');
        }

        return $this->render('admin/fetch_data/index.html.twig', [
            'controller_name' => 'AdminFetchDataController',
            'SettingForm' => $form->createView(),
            'FetchLog' => $LastFetchLog,
        ]);
    }

    #[Route('/admin/fetch_data/run', name: 'admin.fetch_data.run')]
    public function fetch_data(EntityManagerInterface $manager, SettingRepository $settingRepository): Response
    {

        $startTime = microtime(true);
        $setting = $settingRepository->findFirstRecord();
        $setting = $setting[0];

        $url = $setting->getFetchUrl();
        $id_start = $setting->getFetchIdStart();
        $id_end = $setting->getFetchIdEnd();

        list($get_data, $id_end) = $this->get_data_url($id_start, $id_end, $url, $manager);

        foreach ($get_data as $data)
        {
            $newJoke = new NewJokes();
            $newJoke->setJoke($data[1]);
            $newJoke->setSource('FETCH');
            $newJoke->setSourceId($data[0]);
            $newJoke->setSourceUrl($data[3]);
            $manager->persist($newJoke);
            $manager->flush();
        }
        $setting->setFetchIdStart($id_end+1);
        $setting->setFetchIdEnd($id_end+($id_end-$id_start+1));
        $setting->setFetchIdLast($id_end);
        $manager->persist($setting);
        $manager->flush();
        $this->addFlash('success', 'Setting Updated, Successfully');

        $this->addFlash(
            'success',
            'Fetch OK --- success ! : ' . strval(count($get_data)) . ' adds  (in ' . number_format((float)(microtime(true) - $startTime), 2, '.', '') . ' seconds)'
        );


        return $this->redirectToRoute('admin.fetch_data.index');
//        return $this->render('admin/fetch_data/run.html.twig',
//            ['controller_name' => 'AdminFetchDataController']
//        );
    }

    public function get_string_between($string, $start, $end): string
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function my_curl($url):string
    {
        $ch = curl_init();

// set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// grab URL and pass output var
        $output = curl_exec($ch);

// close curl resource, and free up system resources
        curl_close($ch);
        return $output;
    }

    public function get_data_url(int $start, int $end, string $url, $manager): array
    {
        $time_start = microtime(true);
        $max_time = ini_get('max_execution_time');
        $time_max_for_finish =  $time_start - $max_time + 15;
        $get_data = [];
        for ($i = $start; $i <= $end; $i++) {
            $url_id = $url . $i;
            $cpage = $this->my_curl($url_id);

            $parsed = $this->get_string_between($cpage, '<p class="card-text">', '</p>');
            if ($parsed != '') {
                $get_data[] = [$i , html_entity_decode($parsed, ENT_QUOTES | ENT_HTML5, 'UTF-8'), $url, $url_id, new \DateTime()];
            }
            if (microtime(true) - $time_max_for_finish <= 0)
            {
                $end = $i;
                break;
            }

        }
        $this->saveFetchData($get_data, $manager);

        $newFetchLogs = new FetchLogs();
        $newFetchLogs->setUrl($url);
        $newFetchLogs->setIdStart($start);
        $newFetchLogs->setIdEnd($end);
        $newFetchLogs->setDateFetch(new \DateTime());
        $newFetchLogs->setNmbOutput(count($get_data));
        $newFetchLogs->setNmbRequest($end-$start+1);
        $newFetchLogs->setExecTime(floatval(microtime(true) - $time_start));
        $manager->persist($newFetchLogs);
        $manager->flush();

        return array($get_data, $end);
    }

    public function saveFetchData($datas, $manager)
    {
        foreach ($datas as $data)
        {
            $newFetchData = new FetchData();
            $newFetchData->setIdFetch($data[0]);
            $newFetchData->setUrl($data[2]);
            $newFetchData->setUrlFull($data[3]);
            $newFetchData->setContent($data[1]);
            $newFetchData->setCreatedAt($data[4]);
            $newFetchData->setUpdatedAt($data[4]);
            $manager->persist($newFetchData);
            $manager->flush();
        }
    }
}
