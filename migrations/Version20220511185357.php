<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220511185357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE api_logs CHANGE json_in json_in LONGTEXT DEFAULT NULL, CHANGE json_out json_out LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE api_logs CHANGE json_in json_in LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', CHANGE json_out json_out LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\'');
    }
}
