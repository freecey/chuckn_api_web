<?php

namespace App\Entity;

use App\Repository\FetchLogsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FetchLogsRepository::class)
 */
class FetchLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     */
    private $idStart;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idEnd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFetch;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nmbOutput;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nmb_request;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $execTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIdStart(): ?int
    {
        return $this->idStart;
    }

    public function setIdStart(int $idStart): self
    {
        $this->idStart = $idStart;

        return $this;
    }

    public function getIdEnd(): ?int
    {
        return $this->idEnd;
    }

    public function setIdEnd(?int $idEnd): self
    {
        $this->idEnd = $idEnd;

        return $this;
    }

    public function getDateFetch(): ?\DateTimeInterface
    {
        return $this->dateFetch;
    }

    public function setDateFetch(\DateTimeInterface $dateFetch): self
    {
        $this->dateFetch = $dateFetch;

        return $this;
    }

    public function getNmbOutput(): ?int
    {
        return $this->nmbOutput;
    }

    public function setNmbOutput(?int $nmbOutput): self
    {
        $this->nmbOutput = $nmbOutput;

        return $this;
    }

    public function getNmbRequest(): ?int
    {
        return $this->nmb_request;
    }

    public function setNmbRequest(?int $nmb_request): self
    {
        $this->nmb_request = $nmb_request;

        return $this;
    }

    public function getExecTime(): ?float
    {
        return $this->execTime;
    }

    public function setExecTime(?float $execTime): self
    {
        $this->execTime = $execTime;

        return $this;
    }
}
